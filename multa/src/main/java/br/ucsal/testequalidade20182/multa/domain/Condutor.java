package br.ucsal.testequalidade20182.multa.domain;

public class Condutor {

	private Integer numeroCnh;

	private String nome;

	public Condutor(Integer numeroCnh, String nome) {
		super();
		this.numeroCnh = numeroCnh;
		this.nome = nome;
	}

	public Integer getNumeroCnh() {
		return numeroCnh;
	}

	public void setNumeroCnh(Integer numeroCnh) {
		this.numeroCnh = numeroCnh;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Condutor [numeroCnh=" + numeroCnh + ", nome=" + nome + "]";
	}

}
