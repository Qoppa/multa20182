package br.ucsal.testequalidade20182.multa.domain;

public enum TipoMultaEnum {
	LEVE(3), MEDIA(4), GRAVE(5), GRAVISSIMA(7);

	private Integer pontos;

	private TipoMultaEnum(Integer pontos) {
		this.pontos = pontos;
	}

	public Integer getPontos() {
		return pontos;
	}

	public static String listarTiposMulta() {
		String lista = "";
		for (TipoMultaEnum tipoMulta : values()) {
			lista += tipoMulta + ", ";
		}
		return lista.substring(0, lista.length() - 2);
	}
}
