package br.ucsal.testequalidade20182.multa.persistence;

import java.util.ArrayList;

import br.ucsal.testequalidade20182.multa.domain.Condutor;
import br.ucsal.testequalidade20182.multa.exception.RegistroNaoEncontradoException;

public class CondutorDao {

	private ArrayList<Condutor> condutores = new ArrayList<>();

	public void inserir(Condutor condutor) {
		condutores.add(condutor);
	}

	public Condutor encontrarByNumeroCnh(Integer numeroCnh) throws RegistroNaoEncontradoException {
		for (Condutor condutor : condutores) {
			if (condutor.getNumeroCnh().equals(numeroCnh)) {
				return condutor;
			}
		}
		throw new RegistroNaoEncontradoException();
	}

}
