package br.ucsal.testequalidade20182.multa.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");

	public static Date parse(String date) throws ParseException {
		return sdf.parse(date);
	}

	public static String format(Date date) {
		return sdf.format(date);
	}

}
