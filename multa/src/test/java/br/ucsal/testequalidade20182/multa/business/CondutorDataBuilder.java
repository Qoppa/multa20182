package br.ucsal.testequalidade20182.multa.business;

import br.ucsal.testequalidade20182.multa.domain.Condutor;

public class CondutorDataBuilder {
	public static final Integer DEFAULT_CNH = 123456;
	public static final String DEFAULT_NAME = "Ze Silva";

	private String nome = DEFAULT_NAME;
	private Integer numeroCnh = DEFAULT_CNH;

	CondutorDataBuilder() {
	}

	public static CondutorDataBuilder aUser() {
		return new CondutorDataBuilder();
	}

	public CondutorDataBuilder withName(String name) {
		this.nome = name;
		return this;
	}

	public CondutorDataBuilder withCnh(Integer numeroCnh) {
		this.numeroCnh = numeroCnh;
		return this;
	}

	public CondutorDataBuilder but() {
		return CondutorDataBuilder.aUser().withName(nome).withCnh(numeroCnh);
	}

	public Condutor build() {
		return new Condutor(numeroCnh, nome);
	}

}
