package br.ucsal.testequalidade20182.multa.business;

import br.ucsal.testequalidade20182.multa.domain.Condutor;
import br.ucsal.testequalidade20182.multa.domain.Multa;
import br.ucsal.testequalidade20182.multa.domain.TipoMultaEnum;

public class MultaObjectMother {

	public static Multa User() {
		return new Multa(null, "paralela", TipoMultaEnum.GRAVE, new Condutor(000, "nome"));
	}

}
