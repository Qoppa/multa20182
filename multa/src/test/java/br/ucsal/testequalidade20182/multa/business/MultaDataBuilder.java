package br.ucsal.testequalidade20182.multa.business;

import java.util.Date;

import br.ucsal.testequalidade20182.multa.domain.Condutor;
import br.ucsal.testequalidade20182.multa.domain.Multa;
import br.ucsal.testequalidade20182.multa.domain.TipoMultaEnum;

public class MultaDataBuilder {

	public static final Condutor DEFAULT_CONDUTOR = new Condutor(1111, "Pedro");
	public static final TipoMultaEnum DEFAULT_TIPO = TipoMultaEnum.GRAVE;
	public static final String DEFAULT_LOCAL = "Paralela";
	// public static final Date DEFAULT_DATE = Date.;

	private Condutor condutor = DEFAULT_CONDUTOR;
	private TipoMultaEnum tipo = DEFAULT_TIPO;
	private String local = DEFAULT_LOCAL;
	private Date dataHora;

	MultaDataBuilder() {
	}

	public static MultaDataBuilder aUser() {
		return new MultaDataBuilder();
	}

	public MultaDataBuilder withCondutor(Condutor condutor) {
		this.condutor = condutor;
		return this;
	}

	public MultaDataBuilder withTipo(TipoMultaEnum tipo) {
		this.tipo = tipo;
		return this;
	}

	public MultaDataBuilder withLocal(String local) {
		this.local = local;
		return this;
	}

	public MultaDataBuilder withDataHora(Date dataHora) {
		this.dataHora = dataHora;
		return this;
	}

	public MultaDataBuilder but() {
		return MultaDataBuilder.aUser().withCondutor(condutor).withDataHora(dataHora).withLocal(local).withTipo(tipo);
	}

	public Multa build() {
		return new Multa(dataHora, local, tipo, condutor);
	}

}
